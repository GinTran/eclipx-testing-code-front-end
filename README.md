## Introduction

This is a project testing code from Eclipx Group. This is front-end repo which use to get direction from current location to an address and check authorization of user by user's role client. You also need to allow the browser use your location to get direction in map.

## Services
```
Auth0
vue-google-map package
Google API Services: Map API Javascript, Direction API, Places API
```

## Installation

Use [npm] package. From root
```
npm install
npm run serve
```

## Workflow

In Home page, you can get the direction from your current location to an address. You need to search the address in the search box, then select travel mode you want to use such as Driving, Transit, Walking and get direction by click Directions button.

In Check user page, you need to start the Nodejs server in the back-end repo first to enable the API. Then use the credential which I provided to test authentication by user role.

## Credential

```
Admin: 
    User name: admin@gmail.com
    Password: Computer123!

User:
    User name: user@gmai.com
    Password: Computer123!
```