import Vue from 'vue'
import App from './App.vue'
import router from './router'
import { Auth0Plugin } from "./auth";
import './../node_modules/bulma/css/bulma.css';
import * as VueGoogleMaps from 'vue2-google-maps'
import { domain, clientId, audience } from "../auth_config.json";

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyDMYq0XYXe4tFoGqSpA6LhGWHA2X1aN4Wg',
    libraries: 'places',
  }
});

// Install the authentication plugin here
Vue.use(Auth0Plugin, {
  domain,
  clientId,
  audience,
  onRedirectCallback: appState => {
    router.push(
      appState && appState.targetUrl
        ? appState.targetUrl
        : window.location.pathname
    );
  }
});

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')