import axios from "axios"

export default {
    async checkUser(accessToken){
        let res = await axios.get("/api/admin_only", {
            headers: {
                Authorization: `Bearer ${accessToken}`
            }
        });
        return res.data;
    }
}